FROM ubuntu:latest as builder

RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get -y --no-install-recommends install \
        ca-certificates \
        cmake \
        doxygen \
        git \
        graphviz \
        g++ \
        libboost1.58-all-dev \
        libreadline-dev \
        libsodium-dev \
        libssl-dev \
        libzmq3-dev \
        make \
        pkg-config \
    && git clone https://github.com/ipbc-dev/ipbc.git /root/ipbc \
    && cd /root/ipbc \
    && mkdir build \
    && cd build \
    && cmake .. \
    && make
    
FROM ubuntu:latest

WORKDIR /opt/ipbc

RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get install -y --no-install-recommends \
        ca-certificates \
        curl \
        gnupg \
        libboost1.58-all-dev \
        software-properties-common \
    && curl -sL https://deb.nodesource.com/setup_9.x | bash \
    && apt-get install -y nodejs \
    && npm install -g pm2 \
    && apt-get autoclean \
    && apt-get autoremove -y \
    && rm -rf /var/lib/apt/lists/*

COPY --from=builder /root/ipbc/build/src/* /opt/ipbc/

EXPOSE 45433 45434

CMD ["pm2-docker", "process.json"]
