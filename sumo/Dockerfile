FROM ubuntu:latest as builder

RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get -y --no-install-recommends install \
        ca-certificates \
        cmake \
        doxygen \
        git \
        graphviz \
        g++ \
        libboost1.58-all-dev \
        libreadline-dev \
        libsodium-dev \
        libssl-dev \
        libzmq3-dev \
        make \
        pkg-config \
    && git clone https://github.com/sumoprojects/sumokoin.git /root/sumo \
    && cd /root/sumo \
    && rm -rf build \
    && make release-static
    
FROM ubuntu:latest

WORKDIR /opt/sumo

RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get install -y --no-install-recommends \
        ca-certificates \
        curl \
        gnupg \
        libboost1.58-all-dev \
        software-properties-common \
    && curl -sL https://deb.nodesource.com/setup_9.x | bash \
    && apt-get install -y nodejs \
    && npm install -g pm2 \
    && apt-get autoclean \
    && apt-get autoremove -y \
    && rm -rf /var/lib/apt/lists/*

COPY --from=builder /root/sumo/build/release/bin/* /opt/sumo/

EXPOSE 19734 19735

CMD ["pm2-docker", "process.json"]
